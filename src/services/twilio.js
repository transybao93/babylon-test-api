import config from '../config.json';
// Download the helper library from https://www.twilio.com/docs/node/install
// Your Account Sid and Auth Token from twilio.com/console
const accountSid = config.twilio.ACCOUNT_SID;
const authToken = config.twilio.AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

client.messages
.create({
    body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
    from: '+15017122661',
    to: '+15558675310'
})
.then(message => console.log(message.sid))
.done();