import moment from 'moment';
import _ from 'lodash';
import {
    bcryptjs,
    ultilities,
    token
} from '../services';
import * as model from '../model';

class MUser
{

    async encryptPassword(req, res, next)
    {
        try {
            req.encryptPassword = await bcryptjs.genPass(req.body.uPassword);
            next();

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

    async checkEmailExist(req, res, next)
    {
        try {
            let email = req.body.email;
            //- check if email exist, then next()
            let count = model.User.countDocuments({
                eEmail: email
            });

            if(count > 0)
            {
                next();
            }else{
                res.status(404).json({
                    error: true,
                    message: "This email is not exist",
                    data: null
                });
            }

        } catch (error) {
            res.status(500).json({
                message: "Error",
                data: error
            });
        }
    }

}

export default new MUser();
