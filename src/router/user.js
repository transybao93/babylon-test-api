import express from 'express';
import MUser from '../middleware/MUser';
import UserController from '../controller/UserController';
const router = express.Router();

//- register
router.put('/', [
    MUser.encryptPassword
], UserController.register);

//- login
router.post('/', [
    MUser.encryptPassword
], UserController.register);

export default router;