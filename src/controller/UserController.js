import {
    size,
    pick
} from 'lodash';
// import moment from 'moment';
import {
    User
} from '../model/index';
import {
    bcryptjs
} from '../services/index';

class UserController 
{
    //- login
    async login(req, res) {
        try {
            
        } catch (error) {
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }       
    }

    async loginOrRegister(req, res) {
        try {
            let data = pick(req.body, ['uEmail', 'uPassword']);
            if(req.newUser === 1)
            {
                //- is new user
                //- create new account, create new token and send token back
                data.encryptPassword = req.encryptPassword;
                await model.User.create(data).then(response => {
                    res.status(201).json({
                        error: false,
                        message: "Created",
                        data: response
                    });    
                });
                
            }else if(req.newUser === 0){
                //- already register
                //- check email and response to front-end to allow to get in
                //- should i create a token for this?

                res.status(200).json({
                    error: false,
                    data: data
                });
            }
            
            
        } catch (error) {
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }  
    }
    //- register
    async register(req, res) {
        try {
            let data = pick(req.body, ['uName', 'uPassword', 'uEmail']);
            data.encryptPassword = req.encryptPassword;
            res.status(201).json({
                error: false,
                data: data
            });
        } catch (error) {
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }       
    }
    //- share a video
    async createNewSharedVideo(req, res) {
        try {
            
        } catch (error) {
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }       
    }
    //- view all youtube movie list
    async viewAllYoutubeVideo(req, res) {
        try {
            
        } catch (error) {
            console.error(error);
            res.status(500).json({
                error: true,
                data: error
            })
        }       
    }
    
}

export default new UserController();
