import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let YoutubeVideoSchema = new Schema({
    
    userID: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    yvTitle: {
        type: String,
        required: true,
    },
    yvLink: {
        type: String,
        required: true
    },
    yvDescription: {
        type: String,
    }

})

const YoutubeVideo = mongoose.model('YoutubeVideo', YoutubeVideoSchema)
export default YoutubeVideo;
