import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let VoteSchema = new Schema({
    
    userID: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    vUpVote: {
        type: Number,
        enum: [0, 1],
        default: 0
    },
    vDownVote: {
        type: Number,
        enum: [0, 1],
        default: 0
    },
    youtubeVideoID: [{ type: Schema.Types.ObjectId, ref: 'YoutubeVideo' }],

})

const Vote = mongoose.model('Vote', VoteSchema)
export default Vote;
