import mongoose from 'mongoose';
import moment from 'moment';

let Schema = mongoose.Schema;
let UserSchema = new Schema({
    
    uEmail: {
        type: String, 
        required: true, 
        index: true, 
        unique: true,
    },
    uPassword: { type: String, select: false, required: false},

})

const User = mongoose.model('User', UserSchema)
export default User;
